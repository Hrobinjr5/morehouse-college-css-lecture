# Morehouse College CSS Lecture

Introduction to CSS

- [Slides](https://docs.google.com/presentation/d/14lDoSOaUuogJnbzDTWKaC3IX4TBUu0BvLWr7FXB8ysA/edit?usp=sharing)

## Resources

- [Lectures](https://morehouse-dcherry.gitlab.io/advanced-software-engineering/lectures/)

The slides on the lectures page provide the step-by-step instructions as exercises for this repository.


